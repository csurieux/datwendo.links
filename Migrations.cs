﻿using Orchard.Data.Migration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;
using Datwendo.Links.Models;
using Orchard.ContentManagement.MetaData.Models;
using System.Data;

namespace Datwendo.Links {
    public class Migrations : DataMigrationImpl {
        public int Create() {
            ContentDefinitionManager.AlterPartDefinition(typeof(LinksPart).Name,
                builder => builder
                    .Attachable()
                    .WithDescription("Link Part To add a link to your content."));

            ContentDefinitionManager.AlterPartDefinition(typeof(LinksWidgetPart).Name,
                builder => builder
                    .Attachable()
                    .WithDescription("Url Links Widget Part."));

            ContentDefinitionManager.AlterTypeDefinition("LinksWidget",
                cfg => cfg
                    .WithPart("LinksWidgetPart")
                    .WithPart("CommonPart")
                    .WithPart("WidgetPart")
                    .WithSetting("Stereotype", "Widget")
                    );

            ContentDefinitionManager.AlterTypeDefinition("Links",
                cfg => cfg
                    .WithPart("Links")
                    .WithPart("LinksPart")
                    .WithPart("CommonPart")
                       .WithSetting("DateEditorSettings.ShowDateEditor", "True")
                    .WithPart("PublishLaterPart")
                    .WithPart("TitlePart")
                    .WithPart("AutoroutePart", builder => builder
                        .WithSetting("AutorouteSettings.AllowCustomPattern", "True")
                        .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "False")
                        .WithSetting("AutorouteSettings.PatternDefinitions", "[{\"Name\":\"Title\",\"Pattern\":\"Links/{Content.Slug}\",\"Description\":\"my-Links\"}]")
                        .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                    .WithPart("TagsPart")
                    .WithPart("IdentityPart")
                    .Creatable()
                    .Listable()
                    .Securable()
                );

            ContentDefinitionManager.AlterPartDefinition("Links", //same name as contentype to attach them directly to contentitem
                builder => builder
                    .WithField("Sujet", fcfg => fcfg
                        .OfType("TaxonomyField")
                        .WithDisplayName("Sujet")
                        .WithSetting("TaxonomyFieldSettings.Taxonomy", "Sujet")
                        .WithSetting("TaxonomyFieldSettings.LeavesOnly", "true")
                        .WithSetting("TaxonomyFieldSettings.SingleChoice", "false")
                        .WithSetting("TaxonomyFieldSettings.Hint", "Define your content with Taxonomy terms")
                        .WithSetting("TaxonomyFieldSettings.Required", "false"))
                    .WithField("AssociatedContent", f => f
                        .OfType("ContentPickerField")
                        .WithDisplayName("Associated Content")
                        .WithSetting("ContentPickerFieldSettings.Hint", "Choisir un contenu auquel associer ce lien")
                        .WithSetting("ContentPickerFieldSettings.Multiple", "false")
                        .WithSetting("ContentPickerFieldSettings.ShowContentTab", "true")
                        .WithSetting("ContentPickerFieldSettings.DisplayedContentTypes", "Blog"))                        
                    .WithField("Image", f => f
                        .OfType("MediaLibraryPickerField")
                        .WithDisplayName("Image Associée")
                        .WithSetting("MediaLibraryPickerFieldSettings.Hint", "Choisir une image pour ce lien")
                        .WithSetting("MediaLibraryPickerFieldSettings.Multiple", "false"))                        
                    .WithDescription("Links content.")
                    );

            return 1;
        }
    }
}
﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;
using System;
using System.Web;
using Datwendo.Links.Models;
using Orchard;

namespace Datwendo.Links.Drivers
{
    public class LinksPartDriver : ContentPartDriver<LinksPart>
    {
        private const string TemplateName = "Parts/LinksPart";
        private IWorkContextAccessor _workContextAccessor;

        public Localizer T { get; set; }

        public LinksPartDriver(HttpContextBase context, IWorkContextAccessor workContextAccessor)
        {
            _workContextAccessor = workContextAccessor;
            T = NullLocalizer.Instance;
        }

        protected override string Prefix
        {
            get { return "LinksPart"; }
        }

        protected override DriverResult Display(LinksPart part, string displayType, dynamic shapeHelper)
        {
            return Combined(
                ContentShape("Parts_LinksPart",
                    () => shapeHelper.Parts_LinksPart(part)),
                ContentShape("Parts_LinksPart_Summary",
                    () => shapeHelper.Parts_LinksPart_Summary(part)),
                ContentShape("Parts_LinksPart_SummaryAdmin",
                    () => shapeHelper.Parts_LinksPart_SummaryAdmin(part))
                );
        }

        protected override DriverResult Editor(LinksPart part, dynamic shapeHelper)
        {
            return ContentShape("Parts_LinksPart_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(LinksPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, null);

            return Editor(part, shapeHelper);
        }

        protected override void Importing(LinksPart part, ImportContentContext context)
        {
            base.Importing(part, context);

            throw new NotImplementedException();
        }
        protected override void Exporting(LinksPart part, ExportContentContext context)
        {
            base.Exporting(part, context);

            throw new NotImplementedException();
        }
    }
}
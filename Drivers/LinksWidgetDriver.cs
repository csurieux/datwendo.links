﻿using Datwendo.Links.Models;
using Datwendo.Links.Services;
using Datwendo.Links.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.Core.Common.Models;
using Orchard.Localization;
using System.Linq;

namespace Datwendo.Links.Drivers
{
    public class LinksWidgetDriver:ContentPartDriver<LinksWidgetPart>
    {
        private readonly IOrchardServices _orchardServices;
        private readonly ILinksManager _LinksService;

        public Localizer T { get; set; }

        public LinksWidgetDriver(IOrchardServices orchardServices, ILinksManager EditoService)
        {
            _orchardServices = orchardServices;
            _LinksService = EditoService;

            T = NullLocalizer.Instance;
        }

        protected override DriverResult Display(LinksWidgetPart part, string displayType, dynamic shapeHelper)
        {
            var contentItems = _LinksService.GetLinks(part);
            return ContentShape("Parts_LinksWidget",
            	() =>
                {
                    var links = contentItems
                    .OrderByDescending(cr => cr.As<CommonPart>().CreatedUtc)
                    .Take(part.MaxCount);

                var list = shapeHelper.List();
                list.AddRange(links.Select(bp => _orchardServices.ContentManager.BuildDisplay(bp, "Detail")));

                var LinkList = shapeHelper.Parts_Links_List(ContentItems: list);

                return shapeHelper.Parts_LinksWidget(ContentItems: LinkList, LinkWidget: part);
            });
        }

        protected override DriverResult Editor(LinksWidgetPart part, dynamic shapeHelper)
        {
            var model = new LinksWidgetPartWiewModel {
                Queries = _LinksService.GetLinksQueries(),
                QueryId = part.QueryId,
                MaxCount = part.MaxCount
            };

            return ContentShape("Parts_LinksWidget_Edit",
            	() => shapeHelper.EditorTemplate(
            		TemplateName: "Parts/LinksWidget",
                	Model: model,
            		Prefix: Prefix
            	)
            );
        }

        protected override DriverResult Editor(LinksWidgetPart part, IUpdateModel updater, dynamic shapeHelper)
        {
            updater.TryUpdateModel(part, Prefix, null, new string[] { "Queries" });

            if (part.QueryId <= 0)
            {
                updater.AddModelError("QueryId", T("You must select a query."));
            }

            return Editor(part, shapeHelper);
        }
    }
}
﻿using Datwendo.Links.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Core.Common.Models;
using Orchard.Localization;
using Orchard.Projections.Descriptors.Filter;
using Orchard.Projections.Models;
using Orchard.Projections.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace Datwendo.Links.Filters
{
    public class LinksFilter : IFilterProvider
    {
        private readonly IWorkContextAccessor _workContextAccessor;
        private readonly IContentManager _contentManager;
        private readonly RequestContext _requestContext;
        public Localizer T { get; set; }

        public LinksFilter(IWorkContextAccessor workContextAccessor, IContentManager contentManager, RequestContext requestContext)
        {
            _workContextAccessor = workContextAccessor;
            T = NullLocalizer.Instance;
            _contentManager = contentManager;
            _requestContext = requestContext;
        }

       
        public void Describe(DescribeFilterContext describe)
        {
            describe.For("Links", T("Links"), T("Links"))
                .Element("Links attached to current content", T("Links attached current content"), T("Links attached to current content"),
                    (Action<dynamic>)ApplyFilter,
                    (Func<dynamic, LocalizedString>)DisplayFilter,
                    null
                );
        }

        public void ApplyFilter(dynamic context)
        {
            var query = (IHqlQuery)context.Query;
            var Id = GetCurrentContentItemId();
            if (Id < 0) //15
                return;
            var val = string.Format("{{{0}}}", Id);
            var val1 = string.Format("{{{0},", Id);
            var val2 = string.Format(",{0}}}", Id);
            var val3 = string.Format(",{0},", Id);
            Action<IAliasFactory> selector = a => a.ContentPartRecord<FieldIndexPartRecord>().Property("StringFieldIndexRecords", "LinksAssociatedContent");
            Action< IHqlExpressionFactory > filter = x => x.And(y => y.Eq("PropertyName", "Links.AssociatedContent."), 
                                                            z => z.Or(a => a.Eq("Value", val),
                                                                        b => b.Or(c => c.Like("Value", val1,HqlMatchMode.Start), 
                                                                            d => d.Or( e => e.Like("Value", val2, HqlMatchMode.End), f => f.Like("Value", val3, HqlMatchMode.Anywhere)))));
            context.Query.Where(selector, filter);
        }

        private int GetCurrentContentItemId()
        {
            object id;
            if (_requestContext.RouteData.Values.TryGetValue("id", out id))
            {
                int contentId;
                if (int.TryParse(id as string, out contentId))
                    return contentId;
            }
            else if (_requestContext.RouteData.Values.TryGetValue("BlogId", out id))
            {
                int contentId;
                if (int.TryParse(id as string, out contentId))
                    return contentId;
            }
            return -1;
        }

        public LocalizedString DisplayFilter(dynamic context)
        {
            return T("Links attached to current content");
        }
    }
}
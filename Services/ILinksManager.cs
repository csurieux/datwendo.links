﻿using Datwendo.Links.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Projections.Models;
using System.Collections.Generic;


namespace Datwendo.Links.Services
{
    public interface ILinksManager : IDependency
    {
        IEnumerable<ContentItem> GetLinks(LinksWidgetPart part);
        List<QueryPart> GetLinksQueries();
        }
    }
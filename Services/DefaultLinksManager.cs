﻿using Datwendo.Links.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Projections.Models;
using Orchard.Projections.Services;
using System.Collections.Generic;
using System.Linq;
using Orchard.Caching;
using Orchard.Logging;
using System.Web.Mvc;
using Orchard.FileSystems.AppData;
using Orchard.Environment.Configuration;

namespace Datwendo.Links.Services
{
    public class DefaultLinksManager : ILinksManager
    {
        private readonly IWorkContextAccessor _workContextAccessor;
        private readonly IProjectionManager _projectionManager;
        private readonly IOrchardServices _orchardServices;

        public ILogger Logger { get; set; }

        public DefaultLinksManager(IProjectionManager projectionManager
            , IOrchardServices orchardServices
            , IWorkContextAccessor workContextAccessor
            , ICacheManager cacheManager
            , ISignals signals
            , UrlHelper UrlHlp
            , IAppDataFolder appDataFolder
            , ShellSettings shellSettings) {
            _projectionManager = projectionManager;
            _orchardServices = orchardServices;
            _workContextAccessor = workContextAccessor;
            Logger = NullLogger.Instance;
        }

        public List<QueryPart> GetLinksQueries()
        {
            IEnumerable<QueryPart> queryParts = _orchardServices.ContentManager.Query<QueryPart, QueryPartRecord>("Query").List();

            List<QueryPart> LinksQueries = new List<QueryPart>();

            foreach (QueryPart part in queryParts)
            {
                ContentItem contentItem = _projectionManager.GetContentItems(part.Id).FirstOrDefault();
                if (contentItem == null)
                {
                    var isLinks = part.FilterGroups.Where(fg => fg.Filters.Where(f => f.Type == "ContentTypes" && f.State.Contains("Links")).Any()).Any();
                    if (isLinks)
                        LinksQueries.Add(part);
                    continue;
                }

                bool hasTitleParts = contentItem.TypeDefinition.Parts.Where(r => r.PartDefinition.Name == "TitlePart").Any();
                bool hasLinksPart = contentItem.TypeDefinition.Parts.Where(r => r.PartDefinition.Name == "LinksPart").Any();

                if (hasTitleParts && hasLinksPart)
                {
                    LinksQueries.Add(part);
                }
            }

            return LinksQueries;
        }

        public IEnumerable<ContentItem> GetLinks(LinksWidgetPart part)
        {
            return _projectionManager.GetContentItems(part.QueryId).Where(c => c.Is<LinksPart>());
        }
    }
}
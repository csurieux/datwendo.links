﻿using System.Collections.Generic;
using Orchard.Projections.Models;

namespace Datwendo.Links.ViewModels
{
    public class LinksWidgetPartWiewModel
    {
        public IEnumerable<QueryPart> Queries { get; set; }
        public int QueryId { get; set; }
        public int MaxCount { get; set; }
    }
}
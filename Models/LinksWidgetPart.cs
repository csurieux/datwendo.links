﻿using Orchard.ContentManagement;

namespace Datwendo.Links.Models
{
    public class LinksWidgetPart : ContentPart
    {
        public int QueryId
        {
            get { return this.Retrieve(x => x.QueryId); }
            set { this.Store(x => x.QueryId, value); }
        }
        public int MaxCount
        {
            get { return this.Retrieve(x => x.MaxCount); }
            set { this.Store(x => x.MaxCount, value); }
        }
    }
}
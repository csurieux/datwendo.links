﻿using Orchard.ContentManagement;
using Orchard.Core.Title.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datwendo.Links.Models
{
    public class LinksPart : ContentPart
    {
        public string Title
        {
            get {  
                    var tt = this.ContentItem.As<TitlePart>();
                    return tt != null ? tt.Title: string.Empty;
            }            
        }

        public string Url
        {
            get { return this.Retrieve(x => x.Url); }
            set { this.Store(x => x.Url, value); }
        }
    }
}

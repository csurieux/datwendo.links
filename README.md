# Datwendo.Links is an Orchard 1.9.x module. #

* It creates a ContentItem named Links and related Widget. 
For each Link, you may select, with Orchard ContentPicker, any other contentitems for which you want to display related links in a widget.
To use it, you must create Orchard queries, in theses queries, use the new filter to select the current ContentItem.
Then select your new query when you create the Widget

* V 1.0

* Christian Surieux - Datwendo - 2016 
* Apache v2 License